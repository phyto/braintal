# Braintal

Braintal is a program that converts [brainfuck](https://en.wikipedia.org/wiki/Brainfuck) code into [Uxn](https://wiki.xxiivv.com/site/uxntal.html).
It's still in the early prototype phase, but feel free to experiment with it.

Confusingly, the program defaults to outputing uxn (not tal), because it integrates its own assembler. This is because the stock [uxnasm](https://sr.ht/~rabbits/uxn/) assembler sometimes failed to parse the unreasonable number of labels that Braintal creates.

## Use

```sh
cargo run < myfile.bf > myfile.rom
```

You will still need an emulator (e.g. uxncli) to run `myfile.rom`.

For help
```sh
cargo run -- --help
```
The two dashes are needed when running it with `cargo run`.

You can install this to your local user by runnig this inside the project root
```sh
cargo install --path .
```
or you can optionally install this program without `git clone`ing by donig
```sh
cargo install --git https://git.disroot.org/phyto/braintal
```

## Features/completeness

Braintal is able to convert any arbitrary brainfuck code into uxn, so long as it doesn't use up enough memory as to corrupt its own code, and the code can fit into a uxn rom. It should warn you in the latter case (though not the former, so be careful with how much memory your programs use).

If you can find a piece of brainfuck code that runs natively but not as uxn from this program, and doesn't exceed 30,000 bytes of memory, and you don't get warned about it being too large, then please make an issue or contact me!

Some of the optimisations probably __will remove loops that hang indefinitely__ (e.g. `+[+-]` would become `+`). However, they should not otherwise mangle the functionality of the code.

## Speed

The resulting uxn is inherently quite slow, especially when faced off against an optimising bf interpreter, though some optimisations are applied to the brainfuck in an attempt to lessen this.

# Todo
- [ ] Optional uxn minification (sacrifice speed for space)
- [ ] More bf optimisation
- [ ] Stats
- [x] Automated integration testing
  - [x] Integrated native bf interpreter
  - [x] Use input in testing
- [ ] Benchmarking of output
- [x] CLI flags to control what happens to the bf

# Misc.

Licensed as AGPL-3 or later (though the license file isn't included anywhere because I'm lazy, but if someone really wants it I'll add it).
Most of the samples in [tests/samples] were not written by me, and as such are not under the same license as the rest of the program. Some of them contain attribution, others not - apologies if somethig was copied without attribution! Some are really small, and contain no attribution because they are believed to be public domain.

Issues, bug reports, patches, pull requests, grammar fixes etc etc are all extremely welcome. I promise i'm friendly!
The testing suite (`cargo test`) relies on a binary called `uxncli` being installed.