/// A brainfuck operation, or an operation composed of sevral brainfuck operations.
///
/// [Op::LoopStart] and [Op::LoopEnd] contain absolute pointers to the corresponding bracket. They may have their pointers set to `0`, in which case they are "unset" and are to be set later by [self::optimise].
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Op {
	/// (Pointer offset, ammount)
	Add(isize),
	/// (ammount)
	Seek(isize),
	LoopStart(usize),
	LoopEnd(usize),
	Input,
	Output,
	// Here come the non-standard ones
	Nop,
	Set(usize),
}

pub mod interpret;
pub mod optimise;

pub fn str_to_ops(s: &str) -> Vec<Op> {
	let mut r = vec![];

	for c in s.chars() {
		if let Some(x) = char_to_op(c) {
			r.push(x)
		}
	}

	// optimise::set_loop_adresses(&mut r);
	r
}

fn char_to_op(c: char) -> Option<Op> {
	use Op::*;
	Some(match c {
		'+' => Add(1),
		'-' => Add(-1),
		'<' => Seek(-1),
		'>' => Seek(1),
		'[' => LoopStart(0),
		']' => LoopEnd(0),
		',' => Input,
		'.' => Output,
		_ => return None,
	})
}

// /// Serialise a slice of Op's into traditional bf.
// pub fn serialise(ops: &[Op]) -> String {
// 	use Op::*;
// 	let mut r = String::new();

// 	for op in ops {
// 		match *op {
// 			Add(x) => {
// 				let c = if x > 0 { '+' } else { '-' };
// 				for _ in 0..x {
// 					r.push(c)
// 				}
// 			}
// 			Seek(x) => {
// 				let c = if x > 0 { '>' } else { '<' };
// 				for _ in 0..x {
// 					r.push(c)
// 				}
// 			}
// 			LoopStart()
// 			Input => r.push(','),
// 			Output => r.push('.'),
// 		}
// 	}

// 	r
// }
