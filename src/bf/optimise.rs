use super::Op;

/// Do the bare minimum optimisation needed for conversion to Tal
pub fn minimum(ops: &mut Vec<Op>) {
	sanitise(ops);
}

/// Apply reasonable optimisations
pub fn normal(ops: &mut Vec<Op>) {
	while merge_adjacent_adds(ops) ||
		merge_adjacent_seeks(ops) ||
		remove_trailing_deadweight(ops) ||

	//TODO:
	// Copy, Mul...

	set(ops) ||
	purge_comments(ops) ||

	// Cleanup
	sanitise(ops)
	{}
}

pub fn experimental(ops: &mut Vec<Op>) {
	normal(ops);

	sanitise(ops);
}

fn set(ops: &mut Vec<Op>) -> bool {
	let mut acted = false;
	use Op::*;
	for i in 0..ops.len() - 2 {
		if let [LoopStart(..), Add(..), LoopEnd(..)] = ops[i..i + 3] {
			ops[i] = Set(0);
			ops[i + 1] = Nop;
			ops[i + 2] = Nop;
			acted = true;
		}
	}
	for i in 0..ops.len() - 1 {
		if let [Add(..), Set(..)] = ops[i..i + 2] {
			ops[i] = Nop;
		}
		if let [Set(..), Set(..)] = ops[i..i + 2] {
			ops[i] = Nop;
		}
		if let [Set(x), Add(y)] = ops[i..i + 2] {
			ops[i] = Set(y.wrapping_add_unsigned(x) as usize);
			ops[i + 1] = Nop;
		}
	}
	acted
}

fn remove_trailing_deadweight(ops: &mut Vec<Op>) -> bool {
	let mut acted = false;
	let mut count = 0;
	for v in ops.iter().rev() {
		use Op::*;
		match v {
			Add(..) | Seek(..) | Set(..) => count += 1,
			_ => break,
		}
	}
	for _ in 0..count {
		ops.pop();
		acted = true;
	}
	acted
}

fn purge_comments(ops: &mut [Op]) -> bool {
	let mut acted = false;
	use Op::*;
	// Remove starting comment
	if let LoopStart(..) = ops[0] {
		for v in ops.iter_mut() {
			if let LoopEnd(..) = v {
				break;
			}
			*v = Nop;
			acted = true;
		}
	}

	// Remove known-zero comments
	for i in 0..ops.len() - 1 {
		use Op::*;
		if let [Set(0), LoopStart(x)] = ops[i..i + 2] {
			for j in i + 1..=x {
				ops[j] = Nop;
				acted = true;
			}
		}
		if let [LoopEnd(..), LoopStart(x)] = ops[i..i + 2] {
			for j in i + 1..=x {
				ops[j] = Nop;
				acted = true;
			}
		}
	}
	acted
}

fn remove_nops(ops: &mut Vec<Op>) -> bool {
	let mut acted = false;
	let mut len = ops.len();
	let mut i = 0;
	loop {
		if i >= len {
			break;
		}

		use Op::*;
		if let Nop | Add(0) | Seek(0) = ops[i] {
			ops.remove(i);
			len -= 1;
			acted = true;
		} else {
			i += 1;
		}
	}
	acted
}

fn merge_adjacent_adds(ops: &mut Vec<Op>) -> bool {
	let mut acted = false;

	let mut i = 0;
	while i < ops.len() - 1 {
		let op = &ops[i];
		if let Op::Add(amt) = op {
			if let Op::Add(other_amt) = ops[i + 1] {
				ops[i] = Op::Add(amt + other_amt);
				ops.remove(i + 1);
				acted = true;
				continue;
			}
		}
		i += 1;
	}

	acted
}
fn merge_adjacent_seeks(ops: &mut Vec<Op>) -> bool {
	let mut acted = false;

	let mut i = 0;
	while i < ops.len() - 1 {
		let op = &ops[i];
		if let Op::Seek(amt) = op {
			if let Op::Seek(other_amt) = ops[i + 1] {
				ops[i] = Op::Seek(amt + other_amt);
				ops.remove(i + 1);
				acted = true;
				continue;
			}
		}
		i += 1;
	}

	acted
}

/// Given a list of instructions, this function will set the adress of each [LoopStart] and [LoopEnd] to the corresponding one.
/// Also removes any Nop instructions.
/// Here be dragons.
fn sanitise(ops: &mut Vec<Op>) -> bool {
	let acted = remove_nops(ops);
	let mut newops = ops.clone();
	//FIXME: this could do it in less passes
	'outer: for (i, op) in ops.iter().enumerate() {
		if let Op::LoopStart(..) = op {
			let mut nest = 0;
			for (j, other_op) in ops.iter().skip(i + 1).enumerate() {
				if let LoopEnd(..) = other_op {
					if nest == 0 {
						newops[i] = LoopStart(i + j + 1);
						newops[i + j + 1] = LoopEnd(i);
						continue 'outer;
					}
				}
				use Op::*;
				match other_op {
					LoopStart(..) => nest += 1,
					LoopEnd(..) => nest -= 1,
					_ => (),
				}
			}
		}
	}

	*ops = newops;
	acted
}
