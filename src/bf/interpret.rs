use crate::bf::*;

pub fn interp(bf: &[Op], inp: &str) -> Vec<u8> {
	let mut arr = [0u8; 30000];
	let mut p = 0;
	let mut r = Vec::new();
	let mut inp_i = 0;

	let mut i = 0;
	while i < bf.len() {
		use Op::*;
		match bf[i] {
			Add(x) => arr[p] = arr[p].wrapping_add_signed(x as i8),
			Seek(x) => p = p.saturating_add_signed(x),
			LoopStart(x) => {
				if arr[p] == 0 {
					i = x;
					continue;
				}
			}
			LoopEnd(x) => {
				if arr[p] > 0 {
					i = x;
					continue;
				}
			}
			Input => {
				arr[p] = inp.chars().nth(inp_i).unwrap() as u8;
				inp_i += 1;
			}
			Output => r.push(arr[p]),
			Set(x) => arr[p] = x as u8,
			Nop => panic!("Nop encountered at index {} with len {}", i, bf.len()),
		}
		i += 1;
	}

	r
}
