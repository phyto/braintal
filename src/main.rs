use clap::Parser;
use std::io::Write;

/// Program to convert Brainfuck to uxn
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
	/// Outputs Tal without converting it to uxn
	#[arg(long)]
	tal: bool,

	/// Outputs internal bf representation - mostly useful for debugging
	#[arg(long)]
	bf: bool,

	/// How agressively to optimise, where 0 is minimal, 1 is normal, 2 is experimental
	#[arg(short = 'O', default_value_t = 1)]
	optimise: u8,

	/// Crudely interpreits the brainfuck natively, instead of converting
	#[arg(short = 'I', long)]
	interpret: bool,
}

fn main() {
	let args = Args::parse();

	// Take input
	let mut bf = read_stdin();

	// Parse & optimise
	{
		use braintal::bf::optimise;
		match args.optimise {
			0 => optimise::minimum(&mut bf),
			1 => optimise::normal(&mut bf),
			2 => optimise::experimental(&mut bf),
			_ => panic!("weird optimisation setting"),
		}
	}

	if args.bf {
		println!("{:?}", bf);
		return;
	}

	if args.interpret {
		let output = braintal::bf::interpret::interp(&bf, "");
		std::io::stdout().write_all(&output).unwrap();
		return;
	}

	// Convert to tal
	let tal = braintal::uxn::from_ops(&bf);
	if args.tal {
		std::io::stdout().write_all(tal.as_bytes()).unwrap();
		return;
	}

	// Convert to uxn
	let (uxn, errs) = ruxnasm::assemble(tal.as_bytes())
		.expect("Failed to convert tal -> uxn! This is probably a library bug.");
	assert_eq!(&errs, &vec![]); // cheap hack

	// Warn if it might be too big
	if braintal::uxn::check_size_safety(&uxn) {
		eprintln!("WARNING: Program will have <30000 bytes of memory; it may exhibit undefined behaviour!");
	}

	// Output
	std::io::stdout().write_all(&uxn).unwrap();
}

fn read_stdin() -> Vec<braintal::bf::Op> {
	let stdin = std::io::stdin();
	let buffer = std::io::read_to_string(stdin).unwrap();

	braintal::bf::str_to_ops(&buffer)
}
