pub mod bf;
pub mod uxn;

#[cfg(test)]
mod tests {

	// - Read BF and parse into uxn, writing the uxn back into a file
	// - run any bf interpreter, and run a uxn interpreter on the file
	// - (optional) feed in input given by a seperate *.inp file
	// - compare outputs between the BF and the uxn interpreter
	// - (optional)  compare outputs with a *.out file

	use super::bf;
	use super::uxn;

	fn interp_uxn(uxn: std::path::PathBuf, input: String) -> Vec<u8> {
		use std::process::Command;
		use std::process::Stdio;

		let mut x = Command::new("uxncli")
			.env_clear()
			.arg(uxn.into_os_string())
			.stdin(Stdio::piped())
			.stdout(Stdio::piped())
			.spawn()
			.unwrap();

		use std::io::Write;

		let mut stdin = x.stdin.take().expect("failed to get stdin");
		let thread = std::thread::spawn(move || {
			stdin
				.write_all(input.as_bytes())
				.expect("failed to write to stdin");
			dbg!("done writing");
		});

		let output = x.wait_with_output().expect("failed to wait on child");
		thread.join().unwrap();

		output.stdout
	}

	fn file_to_bf(p: &std::path::Path) -> Vec<bf::Op> {
		let f = std::fs::File::open(p).unwrap();
		let buffer = std::io::read_to_string(f).unwrap();

		let mut bf = bf::str_to_ops(&buffer);
		bf::optimise::normal(&mut bf);
		bf
	}

	#[test]
	fn compare_bf_uxn() {
		use std::{fs, io};
		let files = fs::read_dir("tests/samples").unwrap();

		// make our temp dir
		use temp_dir::TempDir;
		let d = TempDir::new().unwrap();

		for f in files {
			let path = f.unwrap().path();
			match path.extension().unwrap().to_str().unwrap() {
				"b" | "bf" => {}
				_ => continue,
			}

			let mut input_path = path.clone();
			input_path.set_extension("input");
			let input_file = fs::File::open(&input_path)
				.unwrap_or_else(|_| fs::File::open("/dev/null").unwrap());
			let input = io::read_to_string(input_file).unwrap();
			eprintln!("Path: {:?}\nInput: {:?}\n", &path, &input_path);

			// make into bf
			let mut bf = file_to_bf(&path);
			let bf_out = bf::interpret::interp(&bf, &input);
			eprintln!("bf done");
			bf::optimise::normal(&mut bf);
			let bf_optimised_out = bf::interpret::interp(&bf, &input);
			eprintln!("optimised bf done");
			assert_eq!(bf_out, bf_optimised_out);

			// make into uxn
			let tal = uxn::from_ops(&bf);
			let (uxn, err) = ruxnasm::assemble(tal.as_bytes()).unwrap();
			assert_eq!(err, vec![]);
			let uxn_file = d.child(path.file_name().unwrap().to_string_lossy());
			std::fs::write(&uxn_file, uxn).unwrap();
			let uxn_out = interp_uxn(uxn_file, input);
			eprintln!("uxn done");

			assert_eq!(bf_out, uxn_out);
		}
	}
}
